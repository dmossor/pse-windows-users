# @summary Creating a User and Group
#
# Creating the User and Group for PSE Onboarding Basics Exercise 3,
# granting the user 'Log on as a Service' rights assignment,
# using the puppetlabs/dsc module as the resource provider.
#
# References:
# https://forge.puppet.com/puppetlabs/dsc
# https://github.com/puppetlabs/puppetlabs-dsc/blob/master/types.md
# https://docs.microsoft.com/en-us/powershell/scripting/dsc/resources/resources?view=powershell-7
#
# @example
#   include pse_windows_users
class pse_windows_users {
  dsc_user { 'pse-user':
    dsc_ensure                   => 'present',
    dsc_username                 => 'pse-user',
    dsc_fullname                 => 'PSE User',
    dsc_description              => 'Windows User for the PSE Onboarding',
    dsc_password                 => {
      'user'     => 'pse-user',
      'password' => Sensitive('PE2018.1.9-pse'),
    },
    dsc_disabled                 => false,
    dsc_passwordchangerequired   => false,
    dsc_passwordchangenotallowed => true,
    dsc_passwordneverexpires     => true,
    dsc_auth_membership          => 'minimum'
  }
  dsc_group { 'PSE Users':
    dsc_ensure    => present,
    dsc_groupname => 'PSE Exercise Users',
    dsc_members   => 'pse-user',
    }
  dsc_userrightsassignment { 'LogonAAService':
    dsc_name     => 'Granting Rights',
    dsc_ensure   => 'present',
    dsc_identity => 'pse-user',
    dsc_policy   => 'Log_on_as_a_service'
  }
}
